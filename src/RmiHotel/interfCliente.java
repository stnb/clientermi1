
package RmiHotel;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;


public interface interfCliente extends Remote{
    public boolean guardar(Cliente cliente) throws RemoteException;
    public boolean actualizar(Cliente cliente) throws RemoteException;
    public boolean eliminar(int id) throws RemoteException;
    public List<Cliente> buscarTodos() throws RemoteException;
    public Cliente buscarCliente(String numDoc) throws RemoteException;
    
    public String Hola()throws RemoteException;
    
}